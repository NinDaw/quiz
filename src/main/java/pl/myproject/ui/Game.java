package pl.myproject.ui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import pl.myproject.dao.DaoManager;
import pl.myproject.entity.Question;

import javax.swing.*;
import java.util.List;


/**
 * Created by RENT on 2017-07-17.
 */
public class Game {
    @FXML
    Label labelQuizName;
    @FXML
    Label labelQuestionNo;
    @FXML
    Label labelAllQuestions;
    @FXML
    Label labelQuestionText;
    @FXML
    RadioButton radioButton1Answer;
    @FXML
    RadioButton radioButton2Answer;
    @FXML
    RadioButton radioButton3Answer;
    @FXML
    RadioButton radioButton4Answer;
    @FXML
    Button buttonNext;
    @FXML
    Button btnGo;
    @FXML
    HBox hBox1;
    @FXML
    HBox hBox2;

    private List<Question> quizList;
    private Stage stage;
    private int quizID;
    private int correctAnswersCounter;
    private int currentQuestionindex;

    public void initialize(Stage stage, int quizID) {
        this.quizList = DaoManager.QUESTION_DAO.getByQuizId(quizID);
        this.stage = stage;
        this.quizID = quizID;
        this.correctAnswersCounter = 0;
        this.currentQuestionindex = 0;

        final ToggleGroup group = new ToggleGroup();
            radioButton1Answer.setToggleGroup(group);
            radioButton2Answer.setToggleGroup(group);
            radioButton3Answer.setToggleGroup(group);
            radioButton4Answer.setToggleGroup(group);

        labelQuizName.setText("WITAJ W QUIZIE!\nPytania z dziedziny: "+DaoManager.QUIZ_DAO.get(quizID).getQuizName());

        btnGo.setOnAction(event -> {
            hBox1.setVisible(true);
            hBox2.setVisible(true);
            labelQuestionText.setVisible(true);
            labelAllQuestions.setText("/" + String.valueOf(quizList.size()));
            buttonNext.setVisible(true);
            fillQuestionForm(currentQuestionindex);
            btnGo.setVisible(false);

        });
        buttonNext.setOnAction(event -> {
            int usersChoice = setUsersChoisenAnswer();
            int index = (Integer.parseInt(labelQuestionNo.getText())) - 1;
            Question question = quizList.get(index);

            if (usersChoice == question.getCorrectAnswer()) {
                correctAnswersCounter++;
                currentQuestionindex++;
                if (currentQuestionindex < quizList.size()) {
                    fillQuestionForm(currentQuestionindex);
                } else {
                    showMessage(correctAnswersCounter);
                }
            } else {
                currentQuestionindex++;
                if (currentQuestionindex < quizList.size()) {
                    fillQuestionForm(currentQuestionindex);
                } else {
                    showMessage(correctAnswersCounter);
                }
            }
            if (group.getSelectedToggle() != null) {
                group.getSelectedToggle().setSelected(false);
            }
        });
    }

    private void showMessage(int correctAnswersCounter) {
        JOptionPane.showMessageDialog(null, "Koniec Quizu!\n Twoj wynik to " + correctAnswersCounter + "/" + quizList.size());
        stage.close();
    }


    private void fillQuestionForm(int i) {
        Question question = quizList.get(i);
        labelQuestionNo.setText(String.valueOf(i + 1));
        labelQuestionText.setText(question.getQuestion());
        radioButton1Answer.setText(question.getAnswers()[0]);
        radioButton2Answer.setText(question.getAnswers()[1]);
        radioButton3Answer.setText(question.getAnswers()[2]);
        radioButton4Answer.setText(question.getAnswers()[3]);
    }

    private int setUsersChoisenAnswer() {
        if (radioButton1Answer.isSelected()) {
            return 1;
        } else if (radioButton2Answer.isSelected()) {
            return 2;
        } else if (radioButton3Answer.isSelected()) {
            return 3;
        } else return 4;
    }
}