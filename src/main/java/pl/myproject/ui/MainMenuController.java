package pl.myproject.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import pl.myproject.dao.DaoManager;
import pl.myproject.dao.QuizDao;
import pl.myproject.entity.Quiz;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pampuchy on 29.06.2017.
 */
public class MainMenuController {
    @FXML
    private Button startButton;
    @FXML
    private Button editorButton;

    @FXML
    private ChoiceBox<Quiz> quizChoiceBox;

    private Stage stage;

    public void initialize(Stage stage) {
        this.stage = stage;

        quizChoiceBox.setConverter(new StringConverter<Quiz>() {
            @Override
            public String toString(Quiz quiz) {
                return quiz.getQuizName();
            }

            @Override
            public Quiz fromString(String string) {
                return null;
            }
        });

        startButton.setOnAction(event -> openGameWindow());
//
//        editorButton.setOnAction(event -> openEditorWindow());
//
//
//        continueButton.setOnAction(event -> {
//            Path filePath = Paths.get("teamId.txt");
//
//            int teamId;
//            try {
//
//                List<String> lines = Files.readAllLines(filePath);
//                teamId = Integer.parseInt(lines.get(0));
//                openGameWindow(teamId);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//        });
//
//        if (DaoManager.MATCH_DAO.getAll().size() > 0) {
//            continueButton.setVisible(true);
//            continueButton.setText("Kontynuuj rozgrywke");
//        } else {
//            continueButton.setVisible(false);
//        }
//        // continueButton.setOnAction(event -> );
//

        fillTeamChoiceBox();
    }

    private void fillTeamChoiceBox() {
        ObservableList<Quiz> quiz = FXCollections.observableArrayList(DaoManager.QUIZ_DAO.getAll());
        quizChoiceBox.setItems(quiz);

        if(quiz.size() > 0) {
            quizChoiceBox.getSelectionModel().select(0);
        }
    }

//    private void openEditorWindow() {
//        Stage editorStage = new Stage();
//        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TeamsEdit.fxml"));
//        HBox root = null;
//        try {
//            root = fxmlLoader.load();
//            TeamsEditController controller = fxmlLoader.getController();
//            controller.initialize(stage);
//            editorStage.setTitle("Football Manager");
//            editorStage.setScene(new Scene(root, 602, 500));
//            editorStage.show();
//
//            editorStage.setOnCloseRequest(event -> fillTeamChoiceBox());
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw new RuntimeException("Błąd FXMLLoadera");
//        }
//    }
//
    private void openGameWindow() {
        Stage editorStage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Game.fxml"));
        Quiz selectedQuiz = quizChoiceBox.getSelectionModel().getSelectedItem();
        if(selectedQuiz == null) {
            return;
        }
        int selectedQuizID = selectedQuiz.getId();
        Parent root = null;
        try {
            root = fxmlLoader.load();
            Game controller = fxmlLoader.getController();
            controller.initialize(editorStage, selectedQuizID);
            editorStage.setTitle("Quiz: "+selectedQuiz.getQuizName());
            editorStage.setScene(new Scene(root, 602, 500));
            editorStage.show();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Błąd FXMLLoadera");
        }
    }
}
