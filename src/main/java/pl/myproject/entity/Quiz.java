package pl.myproject.entity;

/**
 * Created by pampuchy on 29.06.2017.
 */
public class Quiz extends Entity {
    private String quizName;

    public Quiz(Integer id, String quizName) {
        super(id);
        this.quizName = quizName;
    }

    public Quiz(String quizName) {

        this.quizName = quizName;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public Quiz(Quiz quiz) {
        super(quiz.getId());
        this.quizName = quiz.quizName;

    }
}
