package pl.myproject.entity;

/**
 * Created by pampuchy on 29.06.2017.
 */
public class Question extends  Entity {
    private int quizId;
    private String question;
    private String [] answers;
    private int correctAnswer;

    public Question(Integer id, int quizId, String question, String[] answers, int correctAnswer) {
        super(id);
        this.quizId = quizId;
        this.question = question;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
    }

    public Question(int quizId, String question, String[] answers, int correctAnswer) {
        this.quizId = quizId;
        this.question = question;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
    }
    public Question(Question question) {
        super(question.getId());
        this.quizId = question.quizId;
        this.question = question.question;
        this.answers = question.answers;
        this.correctAnswer = question.correctAnswer;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public int getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(int correctAnswer) {
        this.correctAnswer = correctAnswer;
    }
}
