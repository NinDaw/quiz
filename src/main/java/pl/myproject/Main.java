package pl.myproject;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.myproject.ui.MainMenuController;


/**
 * Created by pampuchy on 29.06.2017.
 */
public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ui/MainMenu.fxml"));
        VBox root = fxmlLoader.load();
        MainMenuController controller = fxmlLoader.getController();
        controller.initialize(primaryStage);
        primaryStage.setTitle("QuizzzzWanie");
        primaryStage.setScene(new Scene(root, 602, 500));
        primaryStage.show();
    }

    public static void main(String[] args) {

        launch(args);

    }
}
