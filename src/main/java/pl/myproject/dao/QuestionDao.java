package pl.myproject.dao;

import pl.myproject.entity.Question;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by pampuchy on 29.06.2017.
 */
public class QuestionDao extends AbstractDao<Question> {

    public List<Question> getByQuizId(int id){
        return entities.stream()
                .filter(q -> q.getQuizId()==id)
                .map(q -> copy(q))
                .collect(Collectors.toList());
    }

    @Override
    protected void update(Question entity, Question entityWithNewValues) {
        entity.setQuizId(entityWithNewValues.getQuizId());
        entity.setQuestion(entityWithNewValues.getQuestion());
        entity.setAnswers(entityWithNewValues.getAnswers());
        entity.setCorrectAnswer(entityWithNewValues.getCorrectAnswer());
    }

    @Override
    protected String createFileLineFromEntity(Question entity) {
        return entity.getId()+"/"+
                entity.getQuizId()+"/"+
                entity.getQuestion()+"/"+
                entity.getAnswers()[0]+"/"+
                entity.getAnswers()[1]+"/"+
                entity.getAnswers()[2]+"/"+
                entity.getAnswers()[3]+"/"+
                entity.getCorrectAnswer();
    }

    @Override
    protected Question copy(Question entity) {
        return new Question(entity);
    }

    @Override
    protected String getFileName() {
        return "questions.txt";
    }

    @Override
    protected Question createEntityFromFileLine(String fileLine) {
        String[] lineValues = fileLine.split("/");
        int id = Integer.parseInt(lineValues[0]);
        int quizzId = Integer.parseInt(lineValues[1]);
        String question = lineValues[2];
        String [] answers = new String[4];
        answers[0]=lineValues[3];
        answers[1]=lineValues[4];
        answers[2]=lineValues[5];
        answers[3]=lineValues[6];
        int correctAnswer = Integer.parseInt(lineValues[7]);
        return new Question(id,quizzId,question,answers,correctAnswer);
    }

}
