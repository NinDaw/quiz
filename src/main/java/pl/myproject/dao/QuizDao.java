package pl.myproject.dao;

import pl.myproject.entity.Quiz;

/**
 * Created by pampuchy on 29.06.2017.
 */
public class QuizDao extends AbstractDao<Quiz> {
    @Override
    protected void update(Quiz entity, Quiz entityWithNewValues) {
        entity.setQuizName(entityWithNewValues.getQuizName());
    }

    @Override
    protected String createFileLineFromEntity(Quiz entity) {
        return entity.getId()+"/"+
                entity.getQuizName();
    }

    @Override
    protected Quiz copy(Quiz entity) {
        return new Quiz(entity);
    }

    @Override
    protected String getFileName() {
        return "quiz.txt";
    }

    @Override
    protected Quiz createEntityFromFileLine(String fileLine) {
        String[] lineValues = fileLine.split("/");
        int id = Integer.parseInt(lineValues[0]);
        String name = lineValues[1];

        return new Quiz(id, name);
    }
}
