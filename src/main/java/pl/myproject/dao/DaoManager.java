package pl.myproject.dao;

import pl.myproject.entity.Question;

/**
 * Created by D on 05.05.2017.
 */
public class DaoManager {
    public final static QuizDao QUIZ_DAO = new QuizDao();
    public final static QuestionDao QUESTION_DAO = new QuestionDao();

}
